
let express = require('express');
let app = express();

const appId = makeid(6);

app.get('/', (req, res) => {
    res.json({"id": appId});
});

app.get('/kill', (req, res) => {
    res.status(203).send();
    process.exit(1);
});

let healthResponseCode = 203;
app.get('/unhealthy', (req, res) => {
    healthResponseCode = 500;
    res.json({"id": appId});
});

app.get('/health', (req, res) => {
    res.status(healthResponseCode).send();
});

app.listen(3000, () => console.log('listening on port 3000...'));

function makeid(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}