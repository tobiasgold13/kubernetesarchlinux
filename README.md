# Arch Linux + Kubernetes

> Well, this shall be a short demonstration of kubernetes on an Arch Linux host. As I use kubernetes more and more at work, I wanted to play with it at home, too.

## Minikube

I use minikube as a local kubernetes installation/playground. I use KVM2 driver which seems to work pretty good.

### Packages

I nedded to install the following packages on my installation. Maybe you don't need all of them, maybe I forgot some in the list. Please leave a note if I did.
You may spot dependencies in the list hierarchy or through the notes.

#### Via Pacman
* docker
* docker-compose
* docker-machine (*needed by docker-machine-driver-kvm2*)
* libvirt (*needed by docker-machine-driver-kvm2*)
    * ebtables
* dnsmasq (*needed by docker-machine-driver-kvm2*)
* qemu-headless (*needed somehow by minikube*)

#### Via AUR
* minikube-bin
    * docker-machine-driver-kvm2
* kubectl-bin

### Runtime

Install packages as described above.

Start needed services:
```shell
systemctl start docker.service
systemctl start libvirtd.service
systemctl start virtlogd.service
```

Start minikube
```shell
minikube start --vm-driver=kvm2
```

> Note: I had to add my user to the *libvirt* group, to run minikube as normal user.

Minikube should be configured and running. You can access the Kubernetes Dashboard with the following command. you need to keep your terminal open, while working with the dashboard.
```shell
minikube dashboard
```

### Troubleshooting

Removing *~/.minikube* might cause some trouble if you try to start a new minikube instance. Just run
```shell
minikube delete
```
instead or afterwards. That might fix the issue.
If you still get network or domain errors or something like that, it can be fixed with the following (as root):
```shell
virsh net-list
virsh net-destroy minikube-net
```
Maybe you need to remove the domain (may be shut down):
```shell
virsh list --all
virsh destroy minikube
virsh undefine minikube
```
Also restarting libvirt might no cause any harm:
```shell
systemctl restart libvirtd.service
```

## Example NodeJS App

Just take a look at index.js and you'll se what the app does. It's just a simple demo app using NodeJs + express to return some dummy data.

* "/" returns a JSON object with a generated ID. Will be used later to verify that there are multiple instances running.

TODO describe urls for health checks that will be added later

## Docker

See the Dockerfile for details on how to build the docker image of the sample app.

Build a docker image with the name *kubernetesarchlinux* and run the image in your local docker. The container will be named *testcontainer*. The sample app is running on port 3000 inside the container, but will be accessible at the host on port 3001.

```shell
docker build . -t kubernetesarchlinux
docker run -d --name testcontainer -p 3001:3000 kubernetesarchlinux
```

To run your containers inside minikube's kubernetes, you need to use minikube's docker registry. To point your local docker environment to minikube you can use the following command:

```shell
eval $(minikube docker-env)
```

> You also don't need root priviledges to use minikube's docker, whereas local docker would need root priviledges (either thorugh the root user of the docker user group).

Now you can build the image again and you will se it in the minikube docker registry. :thumbsup:

## Kubernetes Deployment

```shell
kubectl config use-context minikube
minikube service test-service --namespace test-namespace --url
```

## Test running POD